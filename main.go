package main

import (
	"bufio"
	"log"
	"os"
	"os/exec"
)

func main() {
	file := os.Args[1]
	log.Println(file)
	readFiles(file)
}

func readFiles(filename string) {
	file, err := os.OpenFile(filename, os.O_RDONLY, 0755)
	if err != nil {
		log.Fatalln("Error Please enter a file that exists")
	}
	defer file.Close()

	sc := bufio.NewScanner(file)
	for sc.Scan() {
		path := sc.Text()
		log.Println(path)
		wget(path)
	}
}

func wget(path string) error {
	cmd := exec.Command("wget", path)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	return cmd.Run()
}
