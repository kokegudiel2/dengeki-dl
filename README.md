# Dengeki-dl 

It is just a code to test "operating system commands", to read a file that is passed as an argument.

```bash
dengeki-dl file.txt
```

After reading the file with the "`os` and `bufio`" options, the `wget` program is used to proceed to download the links within the file.